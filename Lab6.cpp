#include<iostream>
#include<ctime>
#include<cstdlib>
#include "Header.h"
using namespace std;

int main(){
	int a[4][4] = {};
	char word = 'A';

	srand(time(0));

	for (int x = 0; x < 4; x++) {
		cout << "  "<< word;
		word++;
	}
	cout << endl;
	word = 'A';

	for (int i = 0; i < 4; i++) {
		for (int z = 0; z < 4; z++) {
			a[i][z] = -1;
		}
	}

	//-1 have edge,no cost 
	//0 no edge
	//>0 have edge,have cost 

	for (int i = 0; i < 4; i++) {
		cout <<word;
		word++;
		for (int z = 0; z < 4; z++) {
			a[i][z] = rand()%4;
			cout << "[" << a[i][z] << "]";
		}
		cout << endl;
	}
	
	Graph ob_graph(a); //create graph

	if (ob_graph.isMultigraph()) { //check multigraph
		cout << "Multigraph: Yes\n";
	}
	else {
		cout << "Multigraph: No\n";
	}

	if (ob_graph.isPseudograph()) { //check psedograph
		cout << "Pseudograph: Yes\n";
	}
	else {
		cout << "Pseudograph: No\n";
	}

	if (ob_graph.isDigraph()) { //check digraph
		cout << "Digraph: Yes\n";
	}
	else {
		cout << "Digraph: No\n";
	}

	if (ob_graph.isWeightedgraph()) { //check weighted graph
		cout << "Weighted Graph: Yes\n";
	}
	else {
		cout << "Weighted Graph: No\n";
	}

	if (ob_graph.isCompletedgraph()) { //check completed graph
		cout << "Completed Graph: Yes\n";
	}

	else {
		cout << "Completed Graph: No \n";
	}

	cout << endl << endl;
	cout << "Dykstra" << endl;
	char node;
	cout << "start node: " << endl;
	cin >> node;
	ob_graph.dykstra(a,node);
	return 0;
}