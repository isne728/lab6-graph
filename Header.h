#include<iostream>
#include<vector>
using namespace std;
class Edge {
public:
	Edge(int x, char y) {
		num = x;
		node = y;
		pointer = NULL;
	}
	int num;
	char node;
	Edge* pointer;
};

class Node {
public:
	Node* point_node;
	Edge* point_edge;
	char name_node;

	Node(char y) {
		name_node = y;
		point_node = NULL;
		point_edge = NULL;
	}
};

class Graph {
public:
	Node* point_head;
	Node* point_tail;

	Graph(int a[4][4]);

	void add_node(char name, Edge* edge_head);
	void add_edge(int x, char name, Edge* &edge_head, Edge* &edge_tail);
	bool isMultigraph();
	bool isPseudograph();
	bool isDigraph();
	bool isWeightedgraph();
	bool isCompletedgraph();
	int findShortestpath(int path[], bool checkShortest[]);
	void getShortestpath(int path[], char start);
	void dykstra(int graph[4][4], char start); //which node should it start
};

void Graph::add_node(char name, Edge* edge_head) {
	Node* pointer_node = new Node(name);
	if (point_head == NULL)
	{
		point_head = pointer_node;
		point_tail = point_head;
		pointer_node->point_edge = edge_head;
	}
	else {
		point_tail->point_node = pointer_node; 
		point_tail = pointer_node; 
		pointer_node->point_edge = edge_head;
	}
}

void Graph::add_edge(int x , char name, Edge* &edge_head, Edge* &edge_tail) {
	Edge* pointer_edge = new Edge(x,name);
	if (edge_head == NULL) {
		edge_head = pointer_edge;
		edge_tail = edge_head;
	}
	else {
		edge_tail->pointer = pointer_edge;
		edge_tail = pointer_edge;
	}
	
}

Graph::Graph(int a[4][4]) {
	point_head = NULL;
	point_tail = NULL;
	char node_name = 'A';

	for (int i = 0; i < 4; i++) {
		Edge* edge_head, * edge_tail;
		edge_head = NULL;
		edge_tail = NULL;
		char edge_name = 'A';

		for (int y = 0; y < 4; y++) {
			add_edge(a[i][y], edge_name, edge_head, edge_tail);
			edge_name++;
		}

		add_node(node_name, edge_head);//create node
		node_name++; //update node name

	}
}

bool Graph::isMultigraph() {
	Node* run_node = point_head;
	bool multi_graph = false;
	vector<char> list_edge;

	while (run_node != NULL) {
		Edge* run_edge = run_node->point_edge;
		list_edge.clear();

		while (run_edge != NULL) {
			
			for (int i = 0; i < list_edge.size(); i++) { //check duplicate edge name
				if (run_edge->node == list_edge[i] ) {
					multi_graph = true;
				}
			}

			if (run_edge->num > 0) {
				list_edge.push_back(run_edge->node); //add in list
			}
			
			

			run_edge = run_edge->pointer;
		}

		run_node = run_node->point_node;
	}

	return multi_graph;
}
 
bool Graph::isPseudograph() {
	Node* run_node = point_head;
	bool pseudo_graph = false;

	while (run_node != NULL) {
		Edge* run_edge = run_node->point_edge;

		while (run_edge != NULL) {
			if (run_edge->node == run_node->name_node && run_edge->num > 0) {
				pseudo_graph = true;
			}

			run_edge = run_edge->pointer;
		}

		run_node = run_node->point_node;
	}

	return pseudo_graph;
}

bool Graph::Graph::isDigraph() {
	Node* run_node = point_head;
	bool di_graph = false;

	while (run_node != NULL) {
		Edge* run_edge = run_node->point_edge;

		while (run_edge != NULL) {
			
			//check direction of edge
			Node* run_node2 = point_head;

			while (run_node2 != NULL) {
				Edge* run_edge2 = run_node2->point_edge;

				while (run_edge2 != NULL) {

					if (run_edge2->node == run_node->name_node && run_edge->num != run_edge2->num)
					{
						di_graph = true;
					}
					run_edge2 = run_edge2->pointer;
				}

				run_node2 = run_node2->point_node;
			}



			run_edge = run_edge->pointer;
		}

		run_node = run_node->point_node;
	}

	return di_graph;
}

bool Graph::isWeightedgraph() {
	Node* run_node = point_head;
	bool weightGraph = false;
	
	while (run_node!=NULL) {
		Edge* run_edge = run_node->point_edge;

		while (run_edge != NULL) {
			if (run_edge->num > 0) {
				
				weightGraph = true;
			}

			run_edge = run_edge->pointer;
		}

		run_node = run_node->point_node;
	}

	return weightGraph;
}

bool Graph::isCompletedgraph() {
	Node* run_node = point_head;
	bool completed_graph = true;

	while (run_node != NULL) {
		Edge* run_edge = run_node->point_edge;

		if (run_edge == NULL) {
			completed_graph = false;
		}

		run_node = run_node->point_node;
	}

	return completed_graph;
}

int Graph::findShortestpath(int path[], bool checkShortest[])
{
	int min = INT_MAX, index;

	for (int i = 0; i < 4; i++) {
		if (checkShortest[i] == false && path[i] <= min) {
			min = path[i];
			index = i;
		}		
	}
		
	return index;
}

void Graph::getShortestpath(int path[],char start)
{
	char node = 'A';
	for (int i = 0; i < 4; i++) {
		cout << start << " to " << node << " : " << path[i] << endl;
		node++;
	}		
}


void Graph::dykstra(int graph[4][4], char start)
{
	int path[4]; //shortest path from start to other node
	bool checkShortestpath[4]; //check shortest path
	int start_node;

	if (start=='A') {
		start_node = 0;
	}
	else if (start=='B') {
		start_node = 1;
	}
	else if (start == 'C') {
		start_node = 2;
	}
	else if (start == 'D') {
		start_node = 3;
	}

	
	for (int i = 0; i < 4; i++){ //set initial value
		path[i] = INT_MAX;
		checkShortestpath[i] = false;
		path[start_node] = 0;
	}

	
	for (int i = 0; i < 3; i++) { //find shortest path

		int index = findShortestpath(path, checkShortestpath); //find index of shortest path
		checkShortestpath[index] = true;

 
		for (int x = 0; x < 4; x++) { //find shortest path
			if (!checkShortestpath[x] && graph[index][x] && path[index] != INT_MAX
				&& path[index] + graph[index][i] < path[x]) {
				path[x] = path[index] + graph[index][x];
			}
		}			
	}

	getShortestpath(path,start); //show shortest path
}